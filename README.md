#Tarsius GTK 3.14 Theme

![Tarsius_screenshot](https://github.com/dotovr/tarsius-gtk-theme/blob/master/Screen1.png)


Tarsius is a GTK theme base on [Xgtk](http://kxmylo.deviantart.com/art/Xgtk-theme-gtk-3-14-3-12-465195148)

Metacity theme base on [Libra](http://gnome-look.org/content/show.php/Libra?content=167689)
